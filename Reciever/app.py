import connexion
from connexion import NoContent
import requests
import yaml
import logging
import logging.config
import datetime
import json
import os
from pykafka import KafkaClient

MAX_EVENTS = 10
EVENT_FILE = "events.json"

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

kafka_dict = None

# Functions go here
# Note - need to consider changing these functions AND the .yaml operationIDs to snake case

hostname = "%s:%d" % (app_config['events']['hostname'], app_config['events']['port'])
logger.debug(f"Kafka hostname - {hostname}")
attempt = 1
topic = None
while topic == None and attempt <= app_config['reconnect']['retries']:
    logger.info(f"Attempt {attempt} to connect to Kafka.")
    try:
        client = KafkaClient(hosts=hostname)
        topic = client.topics[str.encode(app_config['events']['topic'])]
    except:
        logger.error("Kafka connection attempt failed.")
        attempt += 1
        if attempt > app_config['reconnect']['retries']:
            logger.error("Max retry count reached. Exiting.")
            raise
        time.sleep(app_config['reconnect']['wait_time'])

def coopRequest(body):
    """Receives a coop party event"""
    logger.info(f'received event <coopRequest> at {datetime.datetime.now()} from player ID {body["player"]}')
    headers = {'Content-Type': 'application/json'}
    # TODO - also log errors and their status codes.
    # client = KafkaClient(hosts=f"{app_config['events']['hostname']}:{app_config['events']['port']}")
    # topic = client.topics[str.encode(app_config['events']['topic'])]
    producer = topic.get_sync_producer()

    msg = {"type": "coop-request",
           "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
           "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info(f'returned event <coopRequest> response at {datetime.datetime.now()} from player ID {body["player"]} '
                f'with status 201. Hopefully.')
    return NoContent, 201


def createListing(body):
    """Receives an item sale listing event"""
    logger.info(f'received event <createListing> at {datetime.datetime.now()} from player ID {body["player"]}')
    headers = {'Content-Type': 'application/json'}
    # client = KafkaClient(hosts=f"{app_config['events']['hostname']}:{app_config['events']['port']}")
    # topic = client.topics[str.encode(app_config['events']['topic'])]
    producer = topic.get_sync_producer()

    msg = {'type': 'listing',
           "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
           "payload": body}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info(f'returned event <createListing> response at {datetime.datetime.now()} from player ID {body["player"]} '
                f'with status 201. Hopefully.')
    return NoContent, 201


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("Game-API.yaml", strict_validation=True, validate_responses=True)

if __name__ == '__main__':
    app.run(port=8081)
