import mysql.connector
import yaml

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())
    db_conf = app_config['datastore']

conn = mysql.connector.connect(host=db_conf['hostname'], user=db_conf['user'], password=db_conf['password'],
                               database=db_conf["db"], port=db_conf['port'])
c = conn.cursor()
# c.execute('''
# CREATE TABLE player
# (id INTEGER PRIMARY KEY ASC,
# username VARCHAR(25) NOT NULL,
# region VARCHAR(255) NOT NULL,
# level INT NOT NULL,
# date_created VARCHAR(100) NOT NULL)
# ''')
#
# c.execute('''
# CREATE TABLE item
# (id INTEGER PRIMARY KEY ASC,
# name VARCHAR(255) NOT NULL,
# category VARCHAR(255) NOT NULL,
# color VARCHAR(255),
# date_created VARCHAR(100) NOT NULL)
# ''')

c.execute('''
CREATE TABLE coop_request
(id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(20) NOT NULL,
description VARCHAR(500) NOT NULL,
slots_free INTEGER NOT NULL,
player INTEGER NOT NULL,
date_created VARCHAR(100) NOT NULL,
CONSTRAINT coop_request_pk PRIMARY KEY (id))
''')

c.execute('''
CREATE TABLE listing
(id INT NOT NULL AUTO_INCREMENT,
price INTEGER NOT NULL,
item INTEGER NOT NULL,
player INTEGER NOT NULL,
date_created VARCHAR(100) NOT NULL,
CONSTRAINT listing_pk PRIMARY KEY (id))
''')

conn.commit()
conn.close()
