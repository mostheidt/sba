import sqlite3

conn = sqlite3.connect('site_data.sqlite')

c = conn.cursor()
# c.execute('''
# CREATE TABLE player
# (id INTEGER PRIMARY KEY ASC,
# username VARCHAR(25) NOT NULL,
# region VARCHAR(255) NOT NULL,
# level INT NOT NULL,
# date_created VARCHAR(100) NOT NULL)
# ''')
#
# c.execute('''
# CREATE TABLE item
# (id INTEGER PRIMARY KEY ASC,
# name VARCHAR(255) NOT NULL,
# category VARCHAR(255) NOT NULL,
# color VARCHAR(255),
# date_created VARCHAR(100) NOT NULL)
# ''')

# noinspection SyntaxError
c.execute('''
CREATE TABLE coop_request
(id INTEGER PRIMARY KEY ASC,
name VARCHAR(20) NOT NULL,
description VARCHAR(500) NOT NULL,
slots_free INTEGER NOT NULL,
player INTEGER NOT NULL,
date_created VARCHAR(100) NOT NULL)
''')

c.execute('''
CREATE TABLE listing
(id INTEGER PRIMARY KEY ASC,
price INTEGER NOT NULL,
item INTEGER NOT NULL,
player INTEGER NOT NULL,
date_created VARCHAR(100) NOT NULL)
''')

conn.commit()
conn.close()
