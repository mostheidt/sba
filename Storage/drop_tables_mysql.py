import mysql.connector
import yaml

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())
    db_conf = app_config['datastore']

conn = mysql.connector.connect(host=db_conf['hostname'], user=db_conf['user'], password=db_conf['password'],
                               database=db_conf["db"], port=db_conf['port'])
c = conn.cursor()

c.execute('''
DROP TABLE coop_request, listing
''')

conn.commit()
conn.close()
