from sqlalchemy import Column, Integer, String, DateTime
from base import Base
import datetime


class Listing(Base):
    """Listing"""

    __tablename__ = "listing"

    id = Column(Integer, primary_key=True)
    price = Column(Integer, nullable=False)
    item = Column(Integer, nullable=False)
    player = Column(Integer, nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, price, item, player):
        """Initialize an item sale listing"""
        self.price = price
        self.item = item
        self.player = player
        self.date_created = datetime.datetime.now()

    def to_dict(self):
        dict = {}
        dict['id'] = self.id
        dict['price'] = self.price
        dict['item'] = self.item
        dict['player'] = self.player
        dict['date_created'] = self.date_created

        return dict
