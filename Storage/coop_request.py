from sqlalchemy import Column, Integer, String, DateTime
from base import Base
import datetime


class CoopRequest(Base):
    """Co-op Request"""

    __tablename__ = "coop_request"

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    description = Column(String(500), nullable=False)
    slots_free = Column(Integer, nullable=False)
    player = Column(Integer, nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, name, description, slots_free, player):
        """Initialize a co-op request"""
        self.name = name
        self.description = description
        self.slots_free = slots_free
        self.player = player
        self.date_created = datetime.datetime.now()

    def to_dict(self):
        """Dictionary Representation of co-op request"""
        dict = {}
        dict['id'] = self.id
        dict['name'] = self.name
        dict['description'] = self.description
        dict['slots_free'] = self.slots_free
        dict['player'] = self.player
        dict['date_created'] = self.date_created

        return dict
