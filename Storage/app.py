import connexion
from connexion import NoContent
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from base import Base
from coop_request import CoopRequest
from listing import Listing
import yaml
# from sqlalchemy import *
import pymysql
import logging
import logging.config
import datetime
import json
from pykafka import KafkaClient
from pykafka.common import OffsetType
from threading import Thread
import os
import time

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())
    db_conf = app_config['datastore']

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

logger.info(f"Connecting to DB {db_conf['db']} at hostname {db_conf['hostname']} and port {db_conf['port']}.")

DB_ENGINE = create_engine(f"mysql+pymysql://{db_conf['user']}:{db_conf['password']}@"
                          f"{db_conf['hostname']}:{db_conf['port']}/{db_conf['db']}", pool_size=30, max_overflow=20)
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)

# Functions go here
# Note - need to consider changing these functions AND the .yaml operationIDs to snake case


def get_coop_requests(start_timestamp, end_timestamp):
    """Gets co-op requests made after the start_timestamp"""
    session = DB_SESSION()

    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    print(start_timestamp_datetime)

    requests = session.query(CoopRequest).filter(and_(CoopRequest.date_created >= start_timestamp_datetime,
        CoopRequest.date_created < end_timestamp_datetime))
    results_list = []
    for request in requests:
        results_list.append(request.to_dict())

    session.close()

    logger.info(f"Query for CoOp Requests after{start_timestamp} returns {len(results_list)} results")

    return results_list, 200


def get_listings(start_timestamp, end_timestamp):
    """Gets item listings created after the start_timestamp"""
    session = DB_SESSION()

    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    print(start_timestamp_datetime)

    listings = session.query(Listing).filter(and_(Listing.date_created >= start_timestamp_datetime,
        Listing.date_created < end_timestamp_datetime))
    results_list = []
    for listing in listings:
        results_list.append(listing.to_dict())

    session.close()

    logger.info(f"Query for Item Listings after {start_timestamp} returns {len(results_list)} results")

    return results_list, 200


def process_messages():
    """Process event messages"""
    hostname = "%s:%d" % (app_config['events']['hostname'], app_config['events']['port'])
    logger.debug(f"Kafka hostname - {hostname}")
    attempt = 1
    topic = None
    while topic == None and attempt <= app_config['reconnect']['retries']:
        logger.info(f"Attempt {attempt} to connect to Kafka.")
        try:
            client = KafkaClient(hosts=hostname)
            topic = client.topics[str.encode(app_config['events']['topic'])]
        except:
            logger.error("Kafka connection attempt failed.")
            time.sleep(app_config['reconnect']['wait_time'])
            attempt += 1
            if attempt > app_config['reconnect']['retries']:
                logger.error("Max retry count reached. Exiting.")
                raise
    # Create a consume on a consumer group that only reads new messages when the service restarts.
    # This is so it doesn't read all the old messages on a restart.
    consumer = topic.get_simple_consumer(consumer_group=b'event_group',
                                         reset_offset_on_start=False,
                                         auto_offset_reset=OffsetType.LATEST)
    # This is blocking - it will wait for a new message.
    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        logger.info(f"Message: {msg}")
        payload = msg['payload']
        if msg["type"] == "coop-request":
            # store coop-request in db
            session = DB_SESSION()
            cr = CoopRequest(payload['name'],
                             payload['description'],
                             payload['slots_free'],
                             payload['player'])
            session.add(cr)
            session.commit()
            item_id = cr.id
            session.close()
            logger.debug(f'Stored event CoopRequest with a unique id of {item_id}')
        elif msg["type"] == "listing":
            # store listing in db
            session = DB_SESSION()
            listing = Listing(payload['price'],
                              payload['item'],
                              payload['player'])
            session.add(listing)
            session.commit()
            # FLAG this line is untested
            item_id = listing.id
            session.close()
            logger.debug(f'stored event Listing with a unique id of {item_id}')
        # commit the new message as being read
        consumer.commit_offsets()


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("Game-API.yaml", strict_validation=True, validate_responses=True)

if __name__ == '__main__':
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()
    app.run(port=8090)
