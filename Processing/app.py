import connexion
from connexion import NoContent
import requests
import yaml
import json
import logging
import logging.config
import datetime
from apscheduler.schedulers.background import BackgroundScheduler
import os.path
from flask_cors import CORS, cross_origin

MAX_EVENTS = 10
EVENT_FILE = "events.json"

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

# Functions go here
# Note - need to consider changing these functions AND the .yaml operationIDs to snake case


def get_stats():
    """Processes and returns stats regarding items created after the timestamp"""
    logger.info("get_stats request received")
    data_path = f"{app_config['datastore']['filename']}"
    if not os.path.isfile(data_path):
        logger.error("Statistics do not exist.")
        return "Statistics do not exist", 404
    with open(data_path) as file:
        data = json.load(file)
    logger.debug(f"Statistics \n"
                 f"num_coop_requests: {data['num_coop_requests']} \n"
                 f"total_coop_slots_free: {data['total_coop_slots_free']} \n"
                 f"num_listings: {data['num_listings']} \n"
                 f"max_price_listing: {data['max_price_listing']} \n")
    logger.info("get_stats request completed")
    return data, 200


def populate_stats():
    """Periodically update statistics"""
    logger.info("Start Periodic Processing")
    data_path = f"{app_config['datastore']['filename']}"
    now_unformatted = datetime.datetime.now()
    now = now_unformatted.strftime("%Y-%m-%dT%H:%M:%SZ")
    if os.path.isfile(data_path):
        with open(data_path) as file:
            data = json.load(file)
    else:
        data = {
            'num_coop_requests': 0,
            'total_coop_slots_free': 0,
            'num_listings': 0,
            'max_price_listing': 0,
            'last_updated': "2000-01-01T00:00:0Z"
        }
    new_coop_requests_response = requests.get(f"{app_config['eventstore1']['url']}?start_timestamp={data['last_updated']}&end_timestamp={now}")
    if new_coop_requests_response.status_code != 200:
        logger.error(f"Querying new coop requests returned status code {new_coop_requests_response.status_code}")
    new_coop_requests = new_coop_requests_response.json()
    # date_created, description, id, name, player, slots_free
    data['num_coop_requests'] += len(new_coop_requests)
    for item in new_coop_requests:
        data['total_coop_slots_free'] += item['slots_free']
    new_listings_response = requests.get(f"{app_config['eventstore2']['url']}?start_timestamp={data['last_updated']}&end_timestamp={now}")
    if new_listings_response.status_code != 200:
        logger.error(f"Querying new listings returned status code {new_listings_response.status_code}")
    new_listings = new_listings_response.json()
    # date_created, id, item, player, price
    data['num_listings'] += len(new_listings)
    for item in new_listings:
        if item['price'] > data['max_price_listing']:
            data['max_price_listing'] = item['price']
    if data['num_listings'] != 0 or data['num_coop_requests'] != 0:
        data['last_updated'] = now
    logger.debug(f"New Statistics \n"
                 f"num_coop_requests: {data['num_coop_requests']} \n"
                 f"total_coop_slots_free: {data['total_coop_slots_free']} \n"
                 f"num_listings: {data['num_listings']} \n"
                 f"max_price_listing: {data['max_price_listing']} \n"
                 f"last_updated: {data['last_updated']}")
    with open(data_path, 'w') as file:
        json.dump(data, file)
    return


def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats,
                  'interval',
                  seconds=app_config['scheduler']['period_sec'])
    sched.start()


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'
app.add_api("Game-API.yaml", strict_validation=True, validate_responses=True)


if __name__ == '__main__':
    init_scheduler()
    app.run(port=8100, use_reloader=False)

